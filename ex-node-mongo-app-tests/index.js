const express = require('express');
const app = express();
const nunjucks = require("nunjucks");
const TEMPLATE_DIR = './src/views/';
const controllers = require("./src/controllers/controller.js");
const models = require("./src/models/model.js");


models.getComposantes().then(async function(data) {
    await controllers.scraping();

    createServer();
}).catch(function() {
    createServer();
});

function createServer() {
    let contentType = 'text/html';

    app.get('/', function(req, res) {
        res.redirect("/annuaire");
    });

    app.get('/annuaire', function(req, res) {
        res.setHeader('Content-Type', contentType);
        let container = nunjucks.render(TEMPLATE_DIR + "index.html");
        controllers.index(container).then(function(data) {
            res.send(data);
            res.end();
        })
    });

    app.use(express.static('web'));

    app.listen(8000);

    console.log("Le serveur écoute sur le port 8000 !")
}


module.exports = app;
