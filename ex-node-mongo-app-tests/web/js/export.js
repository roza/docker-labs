let svg = document.querySelector("svg");
getCSSStyles(svg);

Array.from(document.getElementsByClassName("export")).forEach(function(elem) {
    elem.addEventListener("click", function() {
        exportAnnuaire(this, svg);
    }, false);
})

function exportAnnuaire(element, svg) {
    let serializer = new XMLSerializer();
    let source = serializer.serializeToString(svg);

    let data;
    let link = document.createElement("a");
    link.style.display = "none";
    let filename = "annuaire_iut";

    let format = element.dataset.format;
    switch (format) {
        case 'svg':
            if (!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)) {
                source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
            }

            if (!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)) {
                source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
            }

            source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

            data = 'data:application/xml;charset=utf-8,' + encodeURI(source);
            link.href = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(source);
            link.download = filename + ".svg";

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

            break;
        case 'png':
            let canvas = document.createElement("canvas");
            var svgSize = svg.getBoundingClientRect();
            canvas.width = svgSize.width * 3;
            canvas.height = svgSize.height * 3;
            canvas.style.width = svgSize.width;
            canvas.style.height = svgSize.height;

            let ctx = canvas.getContext("2d");
            ctx.scale(3, 3);

            let img = document.createElement("img");
            img.setAttribute("src", "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(source))));
            img.onload = function() {
                ctx.drawImage(img, 0, 0);
                let canvasdata = canvas.toDataURL("image/png", 1);

                link.download = filename + ".png";
                link.href = canvasdata;

                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            };

            break;
        default:
            console.log('Sorry, we are out of ' + format + '.');
    }
}

function getCSSStyles(parentElement) {
    function appendCSS(cssText, element) {
        let styleElement = document.createElement("style");
        styleElement.setAttribute("type", "text/css");
        styleElement.innerHTML = cssText;
        let refNode = element.hasChildNodes() ? element.children[0] : null;
        element.insertBefore(styleElement, refNode);
    }

    function contains(str, arr) {
        return arr.indexOf(str) === -1 ? false : true;
    }

    let selectorTextArr = [];

    // Add Parent element Id and Classes to the list
    selectorTextArr.push('#' + parentElement.id);
    for (let c = 0; c < parentElement.classList.length; c++)
        if (!contains('.' + parentElement.classList[c], selectorTextArr))
            selectorTextArr.push('.' + parentElement.classList[c]);

    // Add Children element Ids and Classes to the list
    let nodes = parentElement.getElementsByTagName("*");
    for (let i = 0; i < nodes.length; i++) {
        let id = nodes[i].id;
        if (!contains('#' + id, selectorTextArr))
            selectorTextArr.push('#' + id);

        let classes = nodes[i].classList;
        for (let c = 0; c < classes.length; c++)
            if (!contains('.' + classes[c], selectorTextArr))
                selectorTextArr.push('.' + classes[c]);
    }

    // Extract CSS Rules
    let extractedCSSText = "";
    for (let i = 0; i < document.styleSheets.length; i++) {
        let s = document.styleSheets[i];

        try {
            if (!s.cssRules) continue;
        } catch (e) {
            if (e.name !== 'SecurityError') throw e; // for Firefox
            continue;
        }

        let cssRules = s.cssRules;
        for (let r = 0; r < cssRules.length; r++) {
            if (contains(cssRules[r].selectorText, selectorTextArr))
                extractedCSSText += cssRules[r].cssText;
        }
    }

    return appendCSS(extractedCSSText, parentElement);
}