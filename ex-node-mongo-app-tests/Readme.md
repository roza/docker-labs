Annuaire IUT Orléans
====================

[![pipeline status](https://gitlab.com/roza/docker-labs/badges/master/pipeline.svg)](https://gitlab.com/roza/docker-labs/commits/master) [![coverage report](https://gitlab.com/roza/docker-labs/badges/master/coverage.svg)](https://gitlab.com/roza/docker-labs/commits/master)

# Sommaire

1. [Introduction](#1-introduction)
2. [Installation](#2-installation)
    1. [Pré-requis](#21-pré-requis)
    2. [Les commandes](#22-commandes)

--------------------

# 1. Introduction
Liste des outils utilisés : 
- **Docker** (<https://www.docker.com/>) 
- **NodeJS** (<https://nodejs.org/en/>) avec nunjucks, express, mocha, cheerio, etc.
- **MongoDB** (<https://www.mongodb.com/>) : Système de gestion de base de données noSQL orientée documents
- **d3js ** (<https://d3js.org/>)

# 2. Installation
## 2.1. Pré-requis Linux
- **Docker** (<https://docs.docker.com/install/linux/docker-ce/ubuntu/>)

## 2.2. Les commandes
- **Étape 1** : `docker-compose build`
- **Étape 2** : `docker-compose up -d`


