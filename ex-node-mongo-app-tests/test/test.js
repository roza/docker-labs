process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const assert = require('assert');
const server = require("../index.js");
const controllers = require("../src/controllers/controller.js");
const models = require("../src/models/model.js");
let code = 0;

chai.use(chaiHttp);

describe('tests', function() {
    afterEach(function() {
        if (this.currentTest.state == 'failed') {
            code = 1;
        }
    });

    after(async() => {
        process.exit(code);
    });

    describe('models', function() {
        describe('clearDB', function() {
            this.timeout(10000);
            it('models.clearDB() => la base doit être vide', async function() {
                console.log(await models.clearDB())
                assert.equal(await models.clearDB(), true);
            });
        });

        describe('createDB', function() {
            this.timeout(10000);
            it('models.createDB() => la base ne doit être pas vide', async function() {
                let data = [{
                    composantes: {
                        nom: "nom",
                        url: "url"
                    },
                    personnes: [{
                        nom: "nom",
                        titre: "titre",
                        telephone: "telephone",
                        poste: "poste",
                        email: "email"
                    }]
                }];

                assert.notEqual((await models.createDB(data)).length, 0);
            });
        });

        describe('getComposantes', function() {
            this.timeout(10000);
            it('models.getComposantes() => doit renvoyé une liste de une composante', async function() {
                let data = await models.getComposantes();

                assert.equal(data.length, 1);
                assert.notEqual(data[0]._id, null);
                assert.equal(data[0].nom, "nom");
                assert.equal(data[0].url, "url");
            });
        });

        describe('getPersonnes', function() {
            this.timeout(10000);
            it('models.getPersonnes() => doit renvoyé une liste de une personne', async function() {
                let data = await models.getPersonnes();

                assert.equal(data.length, 1);
                assert.notEqual(data[0]._id, null);
                assert.equal(data[0].nom, "nom");
                assert.equal(data[0].titre, "titre");
                assert.equal(data[0].telephone, "telephone");
                assert.equal(data[0].poste, "poste");
                assert.equal(data[0].email, "email");
            });
        });

        describe('getPersonnesByComposante', function() {
            this.timeout(10000);
            it('models.getPersonnesByComposante() => la base doit être vide', async function() {
                let data = await models.getComposantes();
                data = await models.getPersonnesByComposante(data[0]);

                assert.equal(data.length, 1);
                assert.notEqual(data[0]._id, null);
                assert.equal(data[0].nom, "nom");
                assert.equal(data[0].titre, "titre");
                assert.equal(data[0].telephone, "telephone");
                assert.equal(data[0].poste, "poste");
                assert.equal(data[0].email, "email");
            });
        });
    });

    describe('controllers', function() {
        describe('controller', function() {
            describe('scraping', function() {
                this.timeout(10000);
                it('models.clearDB() + controllers.scraping() => la base ne doit pas être vide', async function() {
                    await models.clearDB();
                    await controllers.scraping();
                    let composantes = await models.getComposantes();
                    let personnes = await models.getPersonnes();
                    let personnesByComposante = await models.getPersonnesByComposante(composantes[2]);

                    assert.notEqual(composantes.length, 0);
                    assert.notEqual(personnes.length, 0);
                    assert.notEqual(personnesByComposante.length, 0);
                });
            });

            describe('controller', async function() {
                this.timeout(10000);
                it('controllers.index() => la base ne doit pas être vide', async function() {
                    let template = await controllers.index();
                    assert.equal(typeof template, "string");
                });
            });
        });
    });

    describe('route (index.js)', function() {
        this.timeout(10000);
        it('GET /', function(done) {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});