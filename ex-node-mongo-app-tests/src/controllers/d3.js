function d3(callback, options) {
    let d3_url = 'https://d3js.org/d3.v4.min.js';
    let d3_hierarchy_url = 'https://d3js.org/d3-hierarchy.v1.min.js';
    options = options || {};
    options.selector = options.selector || 'svg';
    options.style = options.style || '';
    return function(args) {
        return '<script src="' + d3_url + '"></script>' +
            '<script src="' + d3_hierarchy_url + '"></script>' +
            '<script>(' + callback + ')(d3, d3.select("' + options.selector + '"), ' + JSON.stringify(args) + ')</script>';
    };
}

module.exports = d3;