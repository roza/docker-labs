const d3 = require('./d3.js');
const models = require("../models/model.js");
const nunjucks = require("nunjucks");

const TEMPLATE_DIR = './src/views/';


class Annuaire {
    constructor(data) {
        this.data = data;
        this.options = {
            selector: "#annuaire"
        };

        this.html = d3(function(d3, svgContainer, data) {
            let i = 0;
            let options = {
                width: 1000,
                height: 875,
                rayonComposante: 64,
                rayonPersonne: 4.5,
                duration: 750,
                couleur: {
                    cercle: "#000000",
                    texteSombre: "#000000",
                    texteClair: "#FFFFFF"
                }
            }

            let svg = svgContainer.append("svg")
                .attr("width", options.width)
                .attr("height", options.height)
                .append("g").attr("transform", "translate(" + (options.width / 2) + "," + (options.height / 2) + ")");

            // declares a tree layout and assigns the size
            let tree = d3.tree()
                .size([2 * Math.PI, Math.min(options.width, options.height) / 2])
                .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

            // Assigns parent, children, height, depth
            let root = d3.hierarchy(data, function(d) { return d.children; });
            root.x0 = 0;
            root.y0 = 0;
            let previousD = root;

            // Collapse after the first level
            collapse(root);
            update(root, previousD);


            // Collapse the node and all it's children
            function collapse(d) {
                if (d.children) {
                    d._children = d.children
                    d._children.forEach(collapse)
                    d.children = null
                }
            }

            function update(source, collapseDest) {
                // Assigns the x and y position for the nodes
                let data = tree(root);

                // Compute the new tree layout.
                let nodes = data.descendants();
                let links = data.descendants().slice(1);

                // Normalize for fixed-depth.
                nodes.forEach(function(d) { d.y = d.depth * 180 });

                // ****************** Nodes section ***************************

                // Update the nodes...
                let node = svg.selectAll('g.node')
                    .data(nodes, function(d) { return d.id || (d.id = ++i); });

                // Enter any new modes at the parent's previous position.
                var nodeEnter = node.enter().append('g')
                    .attr("class", "node")
                    .attr('cursor', function(d) {
                        // if (d.depth <= 1) {
                            return "pointer";
                        // }
                    })
                    .attr("transform", function(d) {
                        return "translate(" + radialPoint(source.x0, source.y0) + ")";
                    })

                    .on('click', function(d) {

                        if (d.depth <= 1) {
                            click(d);
                            if (d.depth == 0){
                                if (!! d.children){
                                  document.getElementById("gestion").classList.add("showGestion");
                                  document.querySelector("#informations > img").src = "/img/IUTO.png";
                                  document.querySelector("#informations > img").style.background = "white";
                                  document.getElementById("nom").innerHTML = "";
                                  document.getElementById("mail").innerHTML = "";
                                  document.getElementById("tel").innerHTML = "";
                                  document.getElementById("poste").innerHTML = "";
                                  document.getElementById("titre").innerHTML = "";
                                }
                                else{
                                    document.getElementById("gestion").classList.remove("showGestion");
                                }

                            }

                        }
                        if (d.depth > 1) {
                            document.querySelector("#informations > img").src = d.data.image;
                            document.getElementById("nom").innerHTML = d.data.nom;
                            document.getElementById("mail").innerHTML = d.data.email;
                            document.getElementById("tel").innerHTML = "Tel : " + d.data.telephone;
                            document.getElementById("poste").innerHTML = "Poste : " +d.data.poste;
                            document.getElementById("titre").innerHTML = "Titre : " +d.data.titre;


                        }
                    });

                // Add Circle for the nodes
                nodeEnter.append('circle')
                    .attr('class', 'node')
                    .attr('r', 1e-6)
                    .style("fill", options.couleur.cercle);

                // Add labels for the nodes
                nodeEnter.append('text')
                    .attr("dy", ".35em")
                    .attr("x", function(d) { return d.depth <= 1 ? 0 : 6; })
                    .attr("text-anchor", function(d) { return (d.depth <= 1) ? "middle" : ((d.x < Math.PI === !d.children) ? "start" : "end"); })
                    .attr("transform", function(d) { return (d.depth == 0) ? null : ("rotate(" + (d.x < Math.PI ? d.x - Math.PI / 2 : d.x + Math.PI / 2) * 180 / Math.PI + ")"); })
                    .style("fill", function(d) {
                        if (d.depth <= 1) {
                            return options.couleur.texteClair;
                        } else {
                            return options.couleur.texteSombre;
                        }
                    })
                    .text(function(d) {
                        if (d.depth == 1) {
                            return d.data.children.length;
                        } else {
                            return d.data.nom;
                        }
                    });

                // UPDATE
                var nodeUpdate = nodeEnter.merge(node);

                // Transition to the proper position for the node
                nodeUpdate.transition()
                    .duration(options.duration)
                    .attr("transform", function(d) {
                        return "translate(" + radialPoint(d.x, d.y) + ")";
                    });

                // Update the node attributes and style
                nodeUpdate.select('circle.node')
                    .attr('r', function(d) {
                        if (d.depth == 0) {
                            return options.rayonComposante;
                        } else if (d.depth == 1) {
                            return d.data.children.length + 10;
                        } else {
                            return options.rayonPersonne;
                        };
                    })
                    .style("fill", options.couleur.cercle);


                // Remove any exiting nodes
                var nodeExit = node.exit().transition()
                    .duration(options.duration)
                    .attr("transform", function(d) {
                        return "translate(" + radialPoint(collapseDest.x0, collapseDest.y0) + ")";
                    })
                    .remove();

                // On exit reduce the node circles size to 0
                nodeExit.select('circle')
                    .attr('r', 1e-6);

                // On exit reduce the opacity of text labels
                nodeExit.select('text')
                    .style('fill-opacity', 1e-6);

                // ****************** links section ***************************

                // Update the links...
                let link = svg.selectAll('path.link')
                    .data(links, function(d) { return d.id; });

                // Enter any new links at the parent's previous position.
                let linkEnter = link.enter().insert('path', "g")
                    .attr("class", "link")
                    .attr('d', function(d) {
                        let o = { x: source.x0, y: source.y0 }
                        return diagonal(o, o)
                    });

                // UPDATE
                let linkUpdate = linkEnter.merge(link);

                // Transition back to the parent element position
                linkUpdate.transition()
                    .duration(options.duration)
                    .attr('d', function(d) {
                        if (d.parent.depth == 0) {
                            return line(d.parent, d);
                        } else {
                            return diagonal(d.parent, d)
                        }
                    });

                // Remove any exiting links
                let linkExit = link.exit().transition()
                    .duration(options.duration)
                    .attr('d', function(d) {
                        let o = { x: collapseDest.x, y: collapseDest.y }
                        return diagonal(o, o)
                    })
                    .remove();

                // Store the old positions for transition.
                nodes.forEach(function(d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                });

                function line(s, d) {
                    let source = radialPoint(s.x, s.y);
                    let destination = radialPoint(d.x, d.y);
                    return "M" + source[0] + "," + source[1] +
                        "L" + destination[0] + "," + destination[1];
                }

                // Creates a curved (diagonal) path from parent to the child nodes
                function diagonal(s, d) {
                    let source = radialPoint(s.x, s.y);
                    let p1 = radialPoint(s.x, (d.y + s.y) / 2);
                    let p2 = radialPoint(d.x, (d.y + s.y) / 2);
                    let destination = radialPoint(d.x, d.y);

                    return "M" + source[0] + "," + source[1] +
                        "C" + p1[0] + "," + p1[1] +
                        " " + p2[0] + "," + p2[1] +
                        " " + destination[0] + "," + destination[1]
                }

                function click(d) {
                    if (d.children) {
                        if (d.depth == 0) {
                            d.children.forEach(collapse);
                        }

                        d._children = d.children;
                        d.children = null;
                        previousD = d;
                    } else {
                        if (d.depth == previousD.depth) {
                            collapse(previousD);
                        }

                        d.children = d._children;
                        d._children = null;
                    }

                    update(d, previousD);
                    previousD = d;
                }

                function radialPoint(x, y) {
                    return [(y = +y) * Math.cos(x -= Math.PI / 2), y * Math.sin(x)];
                }
            }
        }, this.options);
    }

    toString() {
        return this.html(this.data).toString();
    }
}


module.exports = {
    index: async function() {
        let data = {
            "nom": "IUT",
            "children": await models.getComposantes()
        };

        for (let i = 0; i < data.children.length; i++) {
            let personnes = await models.getPersonnesByComposante(data.children[i]);
            data.children[i].children = personnes;
            data.children[i]._children = personnes;
        }

        return nunjucks.render(TEMPLATE_DIR + "index.html", {
            'annuaire': new Annuaire(data)
        });
    },
    export: function(format) {

    },
    scraping: require("./scraping.js").scraping
};
