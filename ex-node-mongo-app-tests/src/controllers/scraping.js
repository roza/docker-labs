const request = require('request-promise');
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');
const models = require("../models/model.js");

const baseUrl = 'http://annuaire.univ-orleans.fr';
const options = {
    uri: baseUrl + '/EmployeParOrg/index/c_oid/905',
    headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1' },
    transform: function (body) {
        return cheerio.load(body);
    }
};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  

module.exports = {
    scraping: function () {
        return request(options)
            .then(function ($) {
                let listePromise = [];
                $('table td a').each(function (i, elem) {
                    let nom = elem.children[0].data.trim();
                    let myRegexp = /\(([a-zA-Z0-9^)]+)\)/;
                    let match = myRegexp.exec(nom);

                    if (match) {
                        nom = match[1]
                    }

                    nom = nom.replace('Département de ', '').replace('Département d\'', '')

                    let composante = {
                        nom: nom,
                        url: baseUrl + elem.attribs.href
                    }

                    options.uri = composante.url;
                    listePromise.push(request(options)
                        .then(sleep(2000))
                        .then(function ($) {
                          let dataAPersonne = [];
                            let listeA = $('table td a');
                            listeA.each(function(index, item) {
                                if (index % 2 == 0) {
                                    dataAPersonne.push(item.attribs.href.substring(29))
                                }
                            });


                            let listePersonnes = [];
                            cheerioTableparser($);
                            let data = $('table').parsetable(false, false, true);
                            if (data.length > 0) {
                                for (let i = 1; i < data[0].length; i++) {
                                    let personne = {
                                        nom: data[0][i],
                                        titre: data[1][i],
                                        image: "http://annuaire.univ-orleans.fr/photo/index/c_uid/" + dataAPersonne[i-1],
                                        telephone: data[2][i],
                                        poste: data[3][i],
                                        email: data[4][i].replace("[remplacer_par_at]", "@")
                                    }
                                    for(let j=1;j<1000;j++);
                                    listePersonnes.push(personne);
                                }
                            }

                            for (var i = 0; i < listePersonnes.length; i++) {
                                listePersonnes[i].composante = composante;
                            }

                            return {
                                composantes: composante,
                                personnes: listePersonnes
                            };

                        })
                        .catch(function (err) {
                            console.log(err)
                            console.log("Erreur récupération composante : " + composante.nom + "(" + composante.url + ")")
                        }));

                })

                return Promise.all(listePromise).then(function (data) {
                    return models.createDB(data);
                })
            })
            .catch(function (err) {
                console.log(err)
                console.log("Erreur récupération composantes")
            });
    }
}
