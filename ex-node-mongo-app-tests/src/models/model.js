const mongoose = require('mongoose');

let mongoDB = 'mongodb://mongo/annuaire';

if (!!process.env['NODE_ENV'] && process.env['NODE_ENV'] == "test") {
    mongoDB = mongoDB + '_test';
}

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


// Schema
let Schema = mongoose.Schema;

let ComposanteSchema = new Schema({
    nom: String,
    url: String,
    children: Array,
    _children: Array
});

let PersonneSchema = new Schema({
    nom: String,
    titre: String,
    telephone: String,
    image: String,
    poste: String,
    email: String,
    composante: [
        { type: Schema.Types.ObjectId, ref: 'Composante' }
    ]
});

// Création Model
let Composante = mongoose.model('Composante', ComposanteSchema);
let Personne = mongoose.model('Personne', PersonneSchema);


module.exports = {
    mongoose: mongoose,
    clearDB: function() {
        return Composante.remove({}).exec().then(function(response) {
            return Personne.remove({}).exec().then(function(response) {
                return true;
            })
        })
    },
    createDB: function(data) {
        return Composante.remove({}).exec().then(function(response) {
            return Personne.remove({}).exec().then(function(response) {
                let listePromiseComposante = [];
                for (let i = 0; i < data.length; i++) {
                    listePromiseComposante.push(Composante.create(data[i].composantes).then(function(composante) {
                        let listePromisePersonne = [];
                        for (let j = 0; j < data[i].personnes.length; j++) {
                            data[i].personnes[j].composante = composante._id
                        }

                        return Personne.create(data[i].personnes);
                    }));
                }

                return Promise.all(listePromiseComposante);
            });
        });

    },
    getComposantes: function() {
        return Composante.find({}, function(err, composantes) {}).sort('nom').exec();
    },
    getPersonnes: function() {
        return Personne.find({}, function(err, personnes) {}).sort('nom').exec();
    },
    getPersonnesByComposante: function(composante) {
        return Personne.find({ "composante": composante._id }, function(err, personnes) {}).sort('nom').exec();
    }
};
